.PHONY: default
default: clean
	clang -masm=intel -e _start -nostdlib -static wg-privkey.c -o wg-privkey
	strip wg-privkey
	wc -c wg-privkey

.PHONY: naivedbg
naivedbg: clean
	clang -masm=intel -g --save-temps -e _main -nostdlib -static wg-privkey.c

.PHONY: debug
debug: clean
	clang -masm=intel -g --save-temps wg-privkey.c

.PHONY: asm
asm:
	clang -masm=intel -e _main -nostdlib -static wg-privkey.c -S
	wc -c wg-privkey.s

.PHONY: clean
clean:
	rm -rf *.dSYM *.out *.o *.s *.i *.bc wg-privkey || true
