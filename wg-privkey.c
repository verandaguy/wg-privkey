#if defined(__APPLE__)
    #if defined(__x86_64__)
const int SYS_EXIT = 0x2000001;
const int SYS_WRITE = 0x2000004;
    #elif defined(__aarch64__)
const int SYS_EXIT = 0x1;
const int SYS_WRITE = 0x4;
    #endif
#elif defined(__linux__)
    #if defined(__x86_64__)
const int SYS_EXIT = 0x3c;
const int SYS_WRITE = 0x1;
    #elif defined(__aarch64__)
const int SYS_EXIT = 0x1;
const int SYS_WRITE = 0x40;
    #endif
#endif

#define WORD_LENGTH 12

// Reimplement a simplified, minimal pow10 to avoid pulling in <math.h>
int pow10(int n) {
    int res = 1;

    for (int i=n; i > 0; i--) {
        res *= 10;
    }

    return res;
}

// Stdlib-free print. Relies on 
static void print_string(char* string) {
    #if defined(__x86_64__)
    asm(
        "mov    rdi, 1\n\t"    // stdout
        "mov    rsi, %0\n\t"   // string pointer
        "mov    rdx, %1\n\t"   // string length
        "mov    rax, %2\n\t"   // load write syscall
        "syscall\n\t"
        :: "m" (string), "i" (WORD_LENGTH), "i" (SYS_WRITE)
    );
    #elif defined(__aarch64__)
    asm(
        "mov    x0, #1\n\t"   // stdout
        "ldr    x1, %0\n\t"   // string pointer
        "mov    x2, %1\n\t"   // string length
        #if defined(__linux__)
        "mov    x8, %2\n\t"   // load write syscall
        #elif defined(__APPLE__)
        "mov    x16, %2\n\t"  // load write syscall
        #endif
        "svc    #0x0\n\t"
        :: "m" (string), "i" (WORD_LENGTH), "i" (SYS_WRITE)
    );
    #endif
}

// Print out "April fools!" and exit
// I hate this preproc derective; solve this later
#if defined(__linux__)
void _start(void) {
#elif defined(__APPLE__)
void start(void) {
#endif
    // split these proto-strings up so we can reasonably use builtin int32s
    // instead of pulling in <stdint.h> for uint64_t
    int iapr = 65112114;
    int iil = 105108032;

    int ifoo = 102111111;
    int ils = 108115033;

    // making this static avoids two memsets which means we can maybe drop a
    // big part of the runtime boilerplate
    static char text[WORD_LENGTH] = {0};

    for (int i = 1; i < 4; i++) {
        text[3-i] = iapr % pow10(3*i) / pow10(3*(i-1));
        text[9-i] = ifoo % pow10(3*i) / pow10(3*(i-1));
        text[6-i] = iil % pow10(3*i) / pow10(3*(i-1));
        text[12-i]= ils % pow10(3*i) / pow10(3*(i-1));
    }
    
    print_string(text);

    #if defined(__x86_64__)
    asm(
        "mov    rbx, 0\n\t"
        "mov    rax, %0\n\t"
        "syscall\n\t"
        :: "i" (SYS_EXIT)
    );
    #elif defined(__aarch64__)
    asm(
        "mov    x0, 0\n\t"
        #if defined(__linux__)
        "mov    x8, %0\n\t"
        #elif defined(__APPLE__)
        "mov    x16, %0\n\t"
        #endif
        "svc    #0x0\n\t"
        :: "i" (SYS_EXIT)
    );
    #endif
}
