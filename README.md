## `wg-privkey`

#### An attempt at humour.

Don't you hate it when you lose your Wireguard private keys?

Isn't it annoying that Wireguard provides `wg genkey` and `wg pubkey` but
doens't complete the set with the obvious `wg privkey`?

Look no further — `wg-privkey` is for you!

- For the long nights spent slouching over the keyboard, trying to get into
  your bastion host that you've locked yourself out of because you can't be
  bothered to properly manage your Wireguard configs...
- For the times you've had to sign into a work server from a new device to
  respond to an incident...
- And even for the times you wish you could jump on a friend's box and leave
  them a surprise in their motd without them realising!

`wg-privkey`'s got you covered.

---

This project is a delayed, mediocre attempt at an April fool's joke. For the
unitiated, [Wireguard](https://www.wireguard.com/), a modern, spartan, ground-
up VPN tool, requires that you generate a privte and public key using the `wg 
genkey` command for the private key, and then the `wg pubkey` command to derive
a public key.

> For readers new to cryptography: public keys are very easy to derive from
> private Diffie-Hellman keys (Wireguard uses _Curve25519_ for this), but
> private keys are impractically difficult to derive from a public key (there's
> [a whole math problem](https://en.wikipedia.org/wiki/Discrete_logarithm) about
> it). This assumption is the foundation of why DH key exhchanges are widely
> trusted.

Because of all of this, the idea that there'd be a practical `wg privkey`
command that could derive a private key based on a public key is either silly
or terrifying depending on how serious the implementor is.

In this case, it's a joke, and it'll likely stay that way until clang lets me
target quantum architectures and I finally figure out how to work with Hilbert
gates.

---

#### How it works

This was partly an exercise for me to figure out how to work with inline
assembly. x86 and ARM are something I've been picking up, but that I'm still
pretty marginally comfortable with, so I thought it'd be a good idea (_Ed.: it
was not._) to contextualise it within C, which I've known for years.

I also combined this with a few other techniques that I haven't used a whole
lot in the past, like manual string obfuscation and header-free code.

- The ugly-but-simple solution I came up with for string obfuscation was
  literally to take the base-10 representations of each char, concatenate them,
  and toss them into stock C `int`s. This format's easily recoverable, and
  avoids tools like `strings` picking it up (though anyone with a disassembler
  or decompiler or who's able to read the stack won't have trouble figuring it
  out)

- For header-free programming, I had to eliminate two functions: `pow`, from
  `math.h` (replaced with an incomplete, minimal, but correct-enough
  implementation in-project), and `printf`.

  `printf` quickly became `extern write`, but I decided to go a bit further and
  started implementing inline assembly which invokes the `write` syscall on a
  small handful of platforms (currently: macOS and Linux on x86_64 and ARM64).
  Dealing with Clang's error messages and the slighty-different `asm`
  constraints between platforms is _not_ fun, but it's definitely a learning
  experience.

  Beyond that, I had to eliminate my use of `uint64_t` from `stdint`, which
  provided an `int` wide enough for me to store the entire words, `April ` and
  `fools!` in just two variables. This got broken up into four ints since I
  reverted to stock `int` (which is _usually_ 32-bits wide, which is good
  enough for me).

  There are almost certainly better ways of doing every part of this
  obfuscation (like using bit shifting to more densely pack the words into
  ints, or using an `unsigned long` to store the words, etc...) but I'll deal
  with those when I have the time. So far the jankiness has kind of grown on
  me.

---

As a disclaimer: you _still_ shouldn't run the version-controlled binary on any
machine you care about, and _definitely_ not on a work machine. If you want to
see it in action, you can have a look at the code and the `Makefile` and go
from there.
